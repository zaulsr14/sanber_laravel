<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sign Up | Sanberbook</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="POST"> 
      @csrf
      <div>
        <label for="firstname"> First Name : </label><br />
        <input name="firstname" type="text" />
      </div>
      <br />
      <div>
        <label for="lastname"> Last Name : </label><br />
        <input name="lastname" type="text" />
      </div>
      <br />
      <div>
        <label for="gender"> Gender : </label><br />
        <input type="radio" name="gender" value="male"> Laki Laki </input> <br />
        <input type="radio" name="gender" value="female"> Perempuan </input> <br />
        <input type="radio" name="gender" value="other"> Other </input> <br />
      </div>
      <br />
      <div>
        <label for="nationality"> Nationality : </label><br />
        <select name="nationality">
          <option value="indonesia">Indonesia</option>
          <option value="malaysia">Malaysia</option>
        </select>
      </div>
      <br />
      <div>
        <label for="language"> Language Spoken : </label><br />
        <input type="checkbox" name="language" value="indonesia"> Bahasa Indonesia </input> <br />
        <input type="checkbox" name="language" value="english"> English </input> <br />
        <input type="checkbox" name="language" value="other"> Other </input> <br />
      </div>
      <br />
      <div>
        <label for="bio">Bio :</label> <br />
        <textarea name="bio" rows="4"></textarea>
      </div>
      <br />
      <div>
        <button type="submit">Sign Up</button>
      </div>
    </form>
  </body>
</html>