CREATE TABLE `customers` (
	`id` INT AUTO_INCREMENT,
	`name` VARCHAR(255),
	`email` VARCHAR(255),
	`password` VARCHAR(255),
	PRIMARY KEY (`id`)
);

CREATE TABLE `orders` (
	`id` INT AUTO_INCREMENT,
	`amount` VARCHAR(255),
	`customer_id` INT,
	PRIMARY KEY (`id`),
  FOREIGN KEY (customer_id) REFERENCES customers(id)
);

INSERT INTO `customers` (name, email, password)
VALUES ('John Doe', 'johndoe@gmail.com', 'john123'),
('John Doe', 'johndoe@gmail.com', 'jenita123');

INSERT INTO `orders` (amount, customer_id)
VALUES (500, 1), (200, 2), (750, 2), (250, 1), (400, 2);

SELECT name as customer_name FROM `customers`
INNER JOIN orders
ON orders.customer_id = customers.id;