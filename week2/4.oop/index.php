<?php 

require "animal.php";
require "ape.php";
require "frog.php";

$sheeps = new Animal("shaun");
$sungokong = new Ape("Kera Sakti");
$kodok = new Frog("buduk");


$sheeps->get_name();
echo "<br />";
echo $sheeps->get_legs();
echo "<br />";
echo $sheeps->get_blood();

$sungokong->get_name();
echo "<br />";
$sungokong->yell();

$kodok->get_name();
echo "<br />";
$kodok->jump();


