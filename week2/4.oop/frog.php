<?php

require_once ('animal.php');

class Frog extends Animal {
  public function __construct($name)
  {
    parent::__construct($name);
    $this->legs = 2;
  }

  public function jump() {
    echo "Hop Hop";
  }

}