<?php

function isPalindrome($string){
    return strrev($string) == $string;
}

function palindrome_angka($angka) {
    $angka++;
    
    while(isPalindrome("$angka")==false){
        $angka++;
    }
    
    return $angka;
}

// TEST CASES
echo palindrome_angka(8); // 9
echo "<br />";
echo palindrome_angka(10); // 11
echo "<br />";
echo palindrome_angka(117); // 121
echo "<br />";
echo palindrome_angka(175); // 181
echo "<br />";
echo palindrome_angka(1000); // 1001
