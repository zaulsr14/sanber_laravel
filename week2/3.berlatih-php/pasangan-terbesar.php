<?php
function pasangan_terbesar($angka){
  $angka_str = strval($angka);
  $angka_length = strlen($angka_str);
  $res = intval($angka_str[0].$angka_str[1]);

  for ($i = 1; $i < $angka_length - 1; $i++) {
    $now = intval($angka_str[$i].$angka_str[$i+1]);

    if ($res < $now) {
      $res = $now;
     }

  }
  return $res;
}

// TEST CASES
echo pasangan_terbesar(641573); // 73
echo pasangan_terbesar(12783456); // 83
echo pasangan_terbesar(910233); // 91
echo pasangan_terbesar(71856421); // 85
echo pasangan_terbesar(79918293); // 99 
